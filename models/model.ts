import * as ORM from 'sequelize';

import {imageModel} from './Image-model';
import {productModel} from './Product-model';
import {orderModel} from './OrderDetail-model';
import {orderProdModel} from './OrderProd-model';

const sequelize = new ORM('postgres://postgres:asdasd@localhost:5432/ShoppingProject');

sequelize.authenticate().then(()=> {
    console.log('Connection to database has been established successfully.');
}).catch(err => {
    console.error('Unable to connect to the database: ', err);
});

export const ProductModel = productModel(sequelize);
export const ImageModel = imageModel(sequelize);
export const OrderModel = orderModel(sequelize);
export const OrderProdModel = orderProdModel(sequelize);

ProductModel.belongsTo(ImageModel, {foreignKey: 'product_id', as: 'imgProduct'});         
ProductModel.belongsToMany(OrderModel, {as: 'products', through: 'orderprod', foreignKey: 'product_id', otherKey: 'order_prodFK'});
OrderModel.belongsToMany(ProductModel, {as: 'orders', through: 'orderprod', foreignKey: 'order_id', otherKey: 'opFK'});
//ProductModel.hasMany(ImageModel, {as: 'imgProduct'});