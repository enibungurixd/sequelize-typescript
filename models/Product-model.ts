import * as ORM from "sequelize";
import { Sequelize } from 'sequelize';


export function productModel(sequelize: Sequelize) {
    return sequelize.define('product', {
        product_id: {
            type: ORM.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        product_name: ORM.STRING,
        product_type: ORM.STRING,
        product_description: ORM.STRING,
        product_price: ORM.INTEGER,
        imgFK: ORM.INTEGER

    },
    {
        timestamps: false,
        tableName: 'product',
        
               
           
    });
   
}
    
