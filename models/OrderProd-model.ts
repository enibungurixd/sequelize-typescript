import * as ORM from "sequelize";
import { Sequelize } from 'sequelize';


export function orderProdModel(sequelize: Sequelize){
    return sequelize.define('orderprod', {
        op_id: {
            type: ORM.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        date_order: ORM.DATE,
    },
    {
        timestamps: false,
        tableName: 'orderprod'
        
});
    }


/*  export const OrderProd = sequelize.define('Product', {
    op_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    date_order: Sequelize.DATE,
    opFK: Sequelize.INTEGER,
    order_prodFK: Sequelize.INTEGER
  });

*/
