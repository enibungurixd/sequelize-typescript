import * as ORM from "sequelize";
import { Sequelize } from "sequelize";

export function orderModel(sequelize: Sequelize){
    return sequelize.define("orderdetail", {
        order_id: {
            type: ORM.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        quantity: ORM.INTEGER,
        total: ORM.DOUBLE,
        date_order: ORM.DATE
    },
    {
        timestamps: false,
        tableName: 'orderdetail',
    });
}

/* const OrderDetail = sequelize.define('Product', {
    order_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    quantity: Sequelize.INTEGER,
    total: Sequelize.DOUBLE,
    date_order: Sequelize.DATE,

}); */

//orderFK: Sequelize.INTEGER
//OrderDetail.belongsToMany(Product, {through: 'OrderProd', foreignKey: 'order_id', otherKey:'order_prodFK'});
