import * as ORM from "sequelize";
import { Sequelize } from 'sequelize';




export function imageModel(sequelize: Sequelize){
    return sequelize.define('image', {
        img_id: {
            type: ORM.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        img_path: ORM.STRING
    },
    {
        timestamps: false,
        tableName: 'image',
        /*classMethods: {
            associate:function(models){
        ImageModel.belongsTo(models.ProductModel, {foreignKey: 'product_id', targetKey: 'imageFK', foreignKeyConstraint: true});
        }   
    }*/
    });
}


/* export const Image = sequelize.define('Product', {
    img_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    img_path: Sequelize.STRING,
    imageFK: Sequelize.INTEGER
});

Image.belongsTo(Product, {foreignKey: 'product_id', targetKey: 'imageFK'});
sequelize.models.Image;*/ 