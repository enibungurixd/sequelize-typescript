
import { Request, Response } from "express";
import { NextFunction } from "connect";
import {ProductModel, ImageModel, OrderModel, OrderProdModel} from '../models/model'
import * as paypal from 'paypal-rest-sdk';
import { keys } from '../keys/keys'

export class Routes {
  public routes(app): void {

    app.get('/api/product', (req: Request, res: Response, next: NextFunction) => {

      const attr = ['product_id', 'product_type', 'product_name', 'product_price' ];
      ProductModel.findAll({
        attributes: attr,
        include: [{
            model: ImageModel,
            attributes: ["img_path"],
            as: 'imgProduct',
        }]  
        }).then(ProductModel => {
        if (!ProductModel) {
          res.status(400).send("notFound");
        } else {
          res.status(200).send(ProductModel);
        }
      });
    });

    app.get('/api/products', (req: Request, res: Response, next: NextFunction) => {
      console.log("Status: " + res.statusCode);

      const attr = ['product_id', 'product_name', 'product_type', 'product_price', 'product_description'];
      ProductModel.findAll({
        attributes: attr,
        include: [{
          model: ImageModel,
          attributes: ["img_path"],
          as: 'imgProduct',
      }]  
      }).then(ProductModel => {
        if (!ProductModel) {
          res.status(400).send("notFound");
        } else {
          res.status(200).send(ProductModel);
        }
      });
    });

    let detail;
    let id1;
    app.post('/api/buy/:id', async(req: Request, res: Response, next: NextFunction) => {
      const id = req.params.id;
      id1 = id;
      paypal.configure({
        'mode': 'sandbox', //sandbox or live 
        'client_id': 'AcJzh9UYp_Pm8cvKpCL1QkTVLopbxB3LD-9Qx6dtOtsCMLbW7Oo0YDfaKN-2Ey1-R5Wheddr7AEdpLxi', // please provide your client id here 
        'client_secret': 'EI7j-RHaHpMmre4DOadQ79SWUXN13Sa7e58MCdc9WoZDW8EW-0EkzmjCOn_vgbrC7T14mAfRg0DNtmG1' // provide your client secret here 
      });

      const orderDetail = {
        quantity: req.body.quantity,
        total: req.body.total
        };
        detail = orderDetail;
        const attr = ['product_id', 'product_name', 'product_type', 'product_price', 'product_description'];
       const product: any = await ProductModel.findOne({
     
          attributes: attr,
            include: [{
              model: ImageModel,
              attributes: ["img_path"],
              as: 'imgProduct'
             }],
                    where: {product_id : id}
        });
        if(!product){
          res.send({message: "error"});
        }else{
         console.log(product);
        }

     const create_payment_json: any = {
       "intent": "sale",
        "payer": {
        "payment_method": "paypal"
        },
        "redirect_urls": {
        // change the URLs
        "return_url": "http://192.168.10.55:5000/api/success",
        "cancel_url": "http://192.168.10.55:5000/api/fail"
        },
        "transactions": [{
          "item_list": {
        // populate the purchased items based on the user-supplied pids and qtys, and other things such as prices and names from DB
            "items": [{
              "name": "item name 1",
              "sku": "001",
              "price": product.product_price,
              "currency": "USD",
              "quantity": orderDetail.quantity
              }]
        },
        "amount": {
            "currency": "USD",
        // calculate the correct amount
        "total": orderDetail.total
        },
        // a good description that you like
        "description": product.product_description
      }]
    };

    paypal.payment.create(create_payment_json, function (error, payment) {
    if (error) {
      console.error(error);
    } else {
      for(var i = 0; i < payment.links.length; i++){
        if(payment.links[i].rel === 'approval_url'){
          res.redirect(payment.links[i].href)
        }
      }
    }
  });
});


  app.get('/api/success',(req: Request, res: Response, next: NextFunction) =>  {
    const payerId = req.query.PayerID;
    const paymentId = req.query.paymentId;

    const execute_payment_json = {
      "payer_id": payerId,
      "transactions": [{
        "amount":{
          "currency": "USD",
          "total": detail.total
        }
      }]
    }

    paypal.payment.execute(paymentId, execute_payment_json, async function(error, payment){
      if(error){
        console.log(error.response);
        throw error;
      }else{
        console.log("Get Payment Response");
        console.log(JSON.stringify(payment));
        const order: any = await OrderModel.create({
          order_id: req.body.order_id,
          quantity: detail.quantity,
          total: detail.total,
          date_order: new Date()
        });
        if(!order)
        res.send({message: "error"});
        else
        res.redirect("http://192.168.10.96:4200/product/"+id1+"/success")        
      }
    });
  });

  app.get('/api/fail', (req: Request, res: Response, next: NextFunction) => {
    res.redirect("http://192.168.10.96:4200/product/"+id1+"/fail");
  });

  app.get('/api/products/:id', async (req: Request, res: Response, next: NextFunction) => {
    console.log("Status: " + res.statusCode);
    const id = req.params.id;
    const attr = ['product_id', 'product_name', 'product_type', 'product_price', 'product_description'];
    
    const products: any = await ProductModel.findOne({
     
      attributes: attr,
        include: [{
          model: ImageModel,
          attributes: ["img_path"],
          as: 'imgProduct'
                }],
                where: {product_id : id}
    });
        if (!products) {
          res.status(400).send("notFound");
        } else {
          res.status(200).send(products);
        }
  });

  app.post("/api/order", async (req: Request, res: Response, next: NextFunction)=> {
      console.log("Status: ", res.statusCode);
   
       const orderDetails = {
        order_id: req.body.order_id,
        quantity: req.body.quantity,
        total: req.body.total,
        date_order: new Date(),
        productFK: req.body.productFK
        };

        console.log(req.body);
        console.log(orderDetails);
        
        const order: any = await OrderModel.create({
          order_id: req.body.order_id,
          quantity: req.body.quantity,
          total: req.body.total,
          date_order: new Date()})

          if(!order){
            res.status(400).send("Request failed");
          }else{
            res.status(200).send(order);
          }
  });
}
}
